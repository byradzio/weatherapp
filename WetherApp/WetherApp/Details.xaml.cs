﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WetherApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Details : Page
    {
        public Details()
        {
            this.InitializeComponent();
            Station.Text = StaicData.station;
            Lat.Text = StaicData.lat + "\"";
            Lon.Text = StaicData.lon + "\"";
            Pressure.Text = StaicData.pressure + "hPa";
            Humidity.Text = StaicData.humidity + "%";
            if (!string.IsNullOrEmpty(StaicData.rainSnow) && string.IsNullOrWhiteSpace(StaicData.rainSnow))
            {
                RainSnow.Text = StaicData.rainSnow + " mm";
            }
            else
            {
                RainSnow.Text = "0 mm";
            }
            Clouds.Text = StaicData.cloudsPercentage + "%";
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }
    }
}
