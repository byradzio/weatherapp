﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WetherApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Wind : Page
    {
        public Wind()
        {
            
           
            this.InitializeComponent();
            RotateTransform rotate = new RotateTransform
            {
                Angle = StaicData.windDeg - 90,
                CenterX = WindArrow.ActualWidth / 2,
                CenterY = WindArrow.ActualHeight / 2
            };
            WindArrow.RenderTransform = rotate;
            WindArrow.Stretch = Stretch.UniformToFill;
            GetDirection();
            WindDirection.Text = GetDirection();
            Speed.Text = StaicData.windSpeed + "m/s";
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private string GetDirection()
        {
            if (StaicData.windDeg >= 348 && StaicData.windDeg < 11) return "N";
            if (StaicData.windDeg >= 11 && StaicData.windDeg < 33) return "NNE";
            if (StaicData.windDeg >= 33 && StaicData.windDeg < 56) return "NE";
            if (StaicData.windDeg >= 56 && StaicData.windDeg < 78) return "ENE";
            if (StaicData.windDeg >= 78 && StaicData.windDeg < 101) return "E";
            if (StaicData.windDeg >= 101 && StaicData.windDeg < 123) return "ESE";
            if (StaicData.windDeg >= 123 && StaicData.windDeg < 146) return "SE";
            if (StaicData.windDeg >= 146 && StaicData.windDeg < 168) return "SSE";
            if (StaicData.windDeg >= 168 && StaicData.windDeg < 191) return "S";
            if (StaicData.windDeg >= 191 && StaicData.windDeg < 213) return "SSW";
            if (StaicData.windDeg >= 213 && StaicData.windDeg < 236) return "SW";
            if (StaicData.windDeg >= 236 && StaicData.windDeg < 258) return "WSW";
            if (StaicData.windDeg >= 258 && StaicData.windDeg < 281) return "W";
            if (StaicData.windDeg >= 281 && StaicData.windDeg < 303) return "WNW";
            if (StaicData.windDeg >= 303 && StaicData.windDeg < 326) return "NW";
            if (StaicData.windDeg >= 326 && StaicData.windDeg < 348) return "NNW";
            return "N/A";
        }
    }
}
