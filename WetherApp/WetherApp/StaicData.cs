﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WetherApp
{
    public static class StaicData
    {
        public static string AllInfo;
        public static string lat;
        public static string lon;
        public static string iconId;
        public static string description;
        public static string station;
        public static string temp;
        public static string pressure;
        public static string humidity;
        public static string temp_min;
        public static string temp_max;
        public static string windSpeed;
        public static int windDeg;
        public static string cloudsPercentage;
        public static DateTime sunRise;
        public static DateTime sunSet;
        public static string rainSnow;

        public static void GetInfo(string info)
        {
            AllInfo = info;
            lon = Regex.Match(info, "(?<=\"lon\":)(.*)(?=,\"lat\")").ToString();
            lat = Regex.Match(info, "(?<=\"lat\":)(.*)(?=},\"weather\")").ToString();
            iconId = Regex.Match(info, "(?<=\\\"icon\\\":\")(.{3})").ToString();
            description = Regex.Match(info, "(?<=\"main\":\")(\\w*)").ToString();
            station = Regex.Match(info, "(?<=\"name\":\")(\\w*)").ToString();
            temp = Regex.Match(info, "(?<=\\\"temp\\\":)(.*)(?=,\"pre)").ToString();
            pressure = Regex.Match(info, "(?<=\"pressure\":)(\\d*)").ToString();
            humidity = Regex.Match(info, "(?<=\"humidity\":)(\\d*)").ToString();
            temp_min = Regex.Match(info, "(?<=\\\"temp_min\\\":)(.*)(?=,\"temp)").ToString();
            temp_max = Regex.Match(info, "(?<=\\\"temp_max\\\":)(.*)(?=},\"v)").ToString();
            windSpeed = Regex.Match(info, "(?<=\"speed\":)(\\d*)").ToString();
            windDeg = Int16.Parse(Regex.Match(info, "(?<=\"deg\":)(\\d*)").ToString());
            cloudsPercentage = Regex.Match(info, "(?<=\"clouds\":{\"all\":)(\\d*)").ToString();
            var _sunRise = double.Parse(Regex.Match(info, "(?<=\"sunset\":)(\\d*)").ToString());
            sunRise = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(_sunRise);
            var _sunset = double.Parse(Regex.Match(info, "(?<=\"sunset\":)(\\d*)").ToString());
            sunSet = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(_sunset);
            rainSnow = Regex.Match(info, "(?<=\"rain\":{\"3h\":)(\\d*)").ToString();
            Debug.WriteLine(info);
        }


    }
}
