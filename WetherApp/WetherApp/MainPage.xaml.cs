﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Web.Http;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WetherApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            GetData();
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
           GetData();
        }

        private async void GetData()
        {
            var accessStatus = await Geolocator.RequestAccessAsync();
            Geolocator geolocator = new Geolocator();
            Geoposition pos = await geolocator.GetGeopositionAsync();
            var lat = pos.Coordinate.Latitude;
            var lon = pos.Coordinate.Longitude;
            var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon +
                      "&units=metric&APPID=94a2a1f354194d31b0596e86168a3774";
            var client = new RestClient(new Uri(url));
            var request = new RestRequest(Method.POST);
            IRestResponse response = await client.Execute(request);
            StaicData.GetInfo(response.Content);
            Station.Text = StaicData.station;
            Temp.Text = "Today: " + StaicData.temp + "°C";
            TempMax.Text = StaicData.temp_max + "°C";
            TempMin.Text = StaicData.temp_min + "°C";
            WindSpeed.Text = "Wind: "+StaicData.windSpeed + " m/s";
            Description.Text = StaicData.description;
            Clouds.Text = "Clouds: "+StaicData.cloudsPercentage + "%";
            if (!string.IsNullOrEmpty(StaicData.rainSnow) && string.IsNullOrWhiteSpace(StaicData.rainSnow))
            {
                Rain.Text = StaicData.rainSnow + " mm";
            }
            else
            {
                Rain.Text = "0 mm";
            }

            WeatherImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Images/" + StaicData.iconId + ".png"));
        }

        private void WindSpeed_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Wind));
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Details));
        }
    }
}
